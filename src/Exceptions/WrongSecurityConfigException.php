<?php

namespace Artel\Support\AutoDoc\Exceptions;

use Exception;

class WrongSecurityConfigException extends Exception
{
}
