<?php

namespace Artel\Support\AutoDoc\Exceptions;

use Exception;

class CanNotParseRulesException extends Exception
{
    public function __construct(string $parameterName, string $requestClass)
    {
        parent::__construct(
            "\nThe rules of {$parameterName} in {$requestClass} can not be automatically converted " .
             "to the description. \n" .
            "Please add a specific parameter description to the annotation of the {$requestClass} manually? \n" .
            "For more details visit: https://gitlab.com/artel-workshop/plugins/laravel-autodoc#annotations \n"
        );
    }
}
