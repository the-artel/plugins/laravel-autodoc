<?php

namespace Artel\Support\AutoDoc\Tests\PhpUnitExtensions;

use Illuminate\Foundation\Application;
use Illuminate\Contracts\Console\Kernel;
use Artel\Support\AutoDoc\Services\SwaggerService;
use PHPUnit\Event\TestRunner\Finished;
use PHPUnit\Event\TestRunner\FinishedSubscriber;
use PHPUnit\Runner\Extension\Extension;
use PHPUnit\Runner\Extension\Facade as EventFacade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;

/**
 * This extension is designed to work with PhpUnit 10
 */
class SwaggerExtensionV10 implements Extension
{
    public function executeAfterLastTest(): void
    {
        $this->createApplication();

        app(SwaggerService::class)->saveProductionData();
    }

    public function bootstrap(Configuration $configuration, EventFacade $facade, ParameterCollection $parameters): void
    {
        $facade->registerSubscriber(new class($this) implements FinishedSubscriber {
            public function __construct(private SwaggerExtensionV10 $thisClass)
            {
            }

            public function notify(Finished $event): void
            {
                $this->thisClass->executeAfterLastTest();
            }
        });
    }

    protected function createApplication(): Application
    {
        $app = require __DIR__ . '/../../../../../../bootstrap/app.php';

        $app->loadEnvironmentFrom('.env.testing');
        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

}
